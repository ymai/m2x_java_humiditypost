Overview
==================
This application is meant to run on RaspberryPI to get data from a Humidity
sensor using a the [Humture](https://bitbucket.org/ymai/humture) device driver.

It will then send the humidity value to the [AT&T M2X](https://m2x.att.com)
platform using the [M2X Java API](https://github.com/attm2x/m2x-java).

Prerequisites
=============
Please download, compile and install the following packages before proceeding.

1. [Humture device driver](https://bitbucket.org/ymai/humture)
2. [M2X Java API](https://github.com/attm2x/m2x-java)

To-Do
======================
Getting binaries
----------------
If not required to compile from the source, the binaries are available in the
download section [here](https://bitbucket.org/ymai/m2x_java_humiditypost/downloads/HumidityPost.one-jar.jar).  Please also download the libDHT library according to instructions
provided [here](https://bitbucket.org/ymai/humture/overview#markdown-header-getting-binaries)

Compile on development machine
------------------------------
1) mvn clean
```bash
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building HumidityPost 1.0.0-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-clean-plugin:2.4.1:clean (default-clean) @ HumidityPost ---
[INFO] Deleting /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 0.498s
[INFO] Finished at: Wed Feb 11 13:41:55 EST 2015
[INFO] Final Memory: 4M/77M
[INFO] ------------------------------------------------------------------------
```

2) mvn package install
```bash
[INFO] Scanning for projects...
[INFO]                                                                         
[INFO] ------------------------------------------------------------------------
[INFO] Building HumidityPost 1.0.0-SNAPSHOT
[INFO] ------------------------------------------------------------------------
[INFO] 
[INFO] --- maven-resources-plugin:2.5:resources (default-resources) @ HumidityPost ---
[debug] execute contextualize
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ HumidityPost ---
[INFO] Compiling 1 source file to /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.5:testResources (default-testResources) @ HumidityPost ---
[debug] execute contextualize
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ HumidityPost ---
[INFO] Compiling 1 source file to /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.10:test (default-test) @ HumidityPost ---
[INFO] Surefire report directory: /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/surefire-reports

-------------------------------------------------------
T E S T S
-------------------------------------------------------
Running com.att.mss.m2m.M2X.AppTest
Tests run: 1, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 0.036 sec

Results :

Tests run: 1, Failures: 0, Errors: 0, Skipped: 0

[INFO] 
[INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ HumidityPost ---
[INFO] Building jar: /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/HumidityPost.jar
[INFO] 
[INFO] --- onejar-maven-plugin:1.4.4:one-jar (default) @ HumidityPost ---
[INFO] Using One-Jar to create a single-file distribution
[INFO] Implementation Version: 1.0.0-SNAPSHOT
[INFO] Using One-Jar version: 0.97
[INFO] More info on One-Jar: http://one-jar.sourceforge.net/
[INFO] License for One-Jar:  http://one-jar.sourceforge.net/one-jar-license.txt
[INFO] One-Jar file: /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/HumidityPost.one-jar.jar
[INFO] 
[INFO] --- maven-resources-plugin:2.5:resources (default-resources) @ HumidityPost ---
[debug] execute contextualize
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/src/main/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:compile (default-compile) @ HumidityPost ---
[INFO] Compiling 1 source file to /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/classes
[INFO] 
[INFO] --- maven-resources-plugin:2.5:testResources (default-testResources) @ HumidityPost ---
[debug] execute contextualize
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/src/test/resources
[INFO] 
[INFO] --- maven-compiler-plugin:2.3.2:testCompile (default-testCompile) @ HumidityPost ---
[INFO] Compiling 1 source file to /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/test-classes
[INFO] 
[INFO] --- maven-surefire-plugin:2.10:test (default-test) @ HumidityPost ---
[INFO] Skipping execution of surefire because it has already been run for this configuration
[INFO] 
[INFO] --- maven-jar-plugin:2.3.2:jar (default-jar) @ HumidityPost ---
[INFO] Building jar: /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/HumidityPost.jar
[INFO] 
[INFO] --- onejar-maven-plugin:1.4.4:one-jar (default) @ HumidityPost ---
[INFO] Using One-Jar to create a single-file distribution
[INFO] Implementation Version: 1.0.0-SNAPSHOT
[INFO] Using One-Jar version: 0.97
[INFO] More info on One-Jar: http://one-jar.sourceforge.net/
[INFO] License for One-Jar:  http://one-jar.sourceforge.net/one-jar-license.txt
[INFO] One-Jar file: /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/HumidityPost.one-jar.jar
[INFO] 
[INFO] --- maven-install-plugin:2.3.1:install (default-install) @ HumidityPost ---
[INFO] Installing /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/target/HumidityPost.jar to /Users/yusufmai/.m2/repository/com/att/mss/m2m/M2X/HumidityPost/1.0.0-SNAPSHOT/HumidityPost-1.0.0-SNAPSHOT.jar
[INFO] Installing /Users/yusufmai/Documents/0users/AppDev/M2X/Java/HumidityPost/pom.xml to /Users/yusufmai/.m2/repository/com/att/mss/m2m/M2X/HumidityPost/1.0.0-SNAPSHOT/HumidityPost-1.0.0-SNAPSHOT.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time: 9.719s
[INFO] Finished at: Wed Feb 11 13:42:58 EST 2015
[INFO] Final Memory: 24M/113M
[INFO] ------------------------------------------------------------------------]
```

3) scp run.sh target/HumidityPost.one-jar.jar <RPI_USER>@<RPI_IP_ADDR>:<your directory>
```bash
<RPI_USER>@<RPI_IP_ADDR>'s password:
run.sh                                                                                       100%  196     0.2KB/s   00:00    
HumidityPost.one-jar.jar                                                                     100%  146KB 145.7KB/s   00:01 
```

Executing on RaspberryPi
------------------------
If the Humture driver has been installed successfully on the RPi using
instructions provided [here](https://bitbucket.org/ymai/humture#markdown-header-on-raspberrypi).
the code should instantiate the Humture driver on pin 1 of the RPi.  If you orient the RPi with
the GPIO on the top, pin 1 is top row 6th from the left.  Power 3.3V is the 1st from the left on the  bottom roll, and ground is 3rd from the left on the top roll.

Modify run.sh according to your M2X device key and device ID.

Then execute the test with the following:

1) sudo -E bash -c ./run.sh
```bash
Press 2 to stop
M2XDeviceStreamTest send thread start
In read
JNI layer pin=1
DHT driver reading from pin=1
Data not good, skip
In read
JNI layer pin=1
DHT driver reading from pin=1
Humidity = 35.0 % Temperature = 23.0 *C (73.4 *F)
Data to send {"value":35}
Sending value 35.000000, result is 202
```
If you see *result is 202*, it means sent is successful.  Or else, you can look
at the API request history from the M2X portal to check what was sent.