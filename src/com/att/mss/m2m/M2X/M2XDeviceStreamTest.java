package com.att.mss.m2m.M2X;

import com.att.m2x.client.M2XClient;
import com.att.m2x.client.M2XDevice;
import com.att.m2x.client.M2XStream;
import com.att.m2x.client.M2XResponse;
import com.att.mss.m2m.device.iDHT;
import com.att.mss.m2m.device.DHTFactory;
import org.slf4j.*;

import java.io.Console;

import org.json.JSONObject;

/*TODO: add java util logging*/

public class M2XDeviceStreamTest implements Runnable
{

	private String			_deviceAPIKey;
	private String			_deviceKey;
	private String			_streamName;
	private static boolean	CONTINUE		= true;
	private static Logger	LOGGER			= LoggerFactory.getLogger("M2XDeviceStreamTest");
	private M2XStream		_stream;
	private int				_sendInterval;
	private int				_threadWaitTime	= 500;

	// Send Interval is the frequency of uploading data in seconds.
	public M2XDeviceStreamTest(String deviceAPIKey, String deviceKey, String streamName, int sendInterval)
	{
		// SETUP M2XClient connection
		_deviceAPIKey = deviceAPIKey;
		_deviceKey = deviceKey;
		_streamName = streamName;
		initializeDeviceStream();
		_sendInterval = sendInterval * 1000 / _threadWaitTime;
	}

	private boolean initializeDeviceStream()
	{
		M2XClient client;
		M2XDevice device;
		client = new M2XClient(_deviceAPIKey);
		device = client.device(_deviceKey);
		_stream = device.stream(_streamName);
		if (_stream == null)
			return false;
		return true;
	}

	private int send(float humidity)
	{
		// Sending device stream data
		if (_stream == null)
		{
			if (!initializeDeviceStream())
				return 408;
		}
		JSONObject humidityValue = new JSONObject();
		humidityValue.put("value", humidity);
		try
		{
			LOGGER.info("Data to send {}", humidityValue.toString());
			M2XResponse res = _stream.updateValue(humidityValue.toString());
			return res.status;
		} catch (Exception e)
		{
			LOGGER.error("M2X stream update error", e);
			// e.printStackTrace(System.out);
		}
		return -1;
	}

	public void run()
	{
		int i = _sendInterval;
		iDHT dht = DHTFactory.create(1);
		try
		{
			LOGGER.info("M2XDeviceStreamTest send thread start");
			while (CONTINUE)
			{
				if (i >= _sendInterval)
				{
					i = 0;
					dht.read();
					float humidity = dht.getHumidity();
					if (humidity > 0)
					{
						int result = send(humidity);
						LOGGER.info("Sending value {}, result is {}", humidity, result);
					}
				}
				// Pause for some time and check again.
				Thread.sleep(_threadWaitTime);
				i++;
			}
			LOGGER.info("M2XDeviceStreamTest send thread end");
		} catch (InterruptedException e)
		{
			LOGGER.error("Thread is interrupted", e);
			//e.printStackTrace(System.out);
		}
	}

	public static void main(String[] args)
	{
//		LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
//	    StatusPrinter.print(lc);

		if (args.length < 4)
		{
			LOGGER.info("Usage: java -jar HumidityPost.one-jar.jar <key> <deviceId> <stream name> <transmit interval>");
			System.exit(1);
		}
		LOGGER.info("\n\tkey={}\n\tdevice={}\n\tstream={}\n\ttransmit interval={}", args[0], args[1],
				args[2], args[3]);

		M2XDeviceStreamTest worker = new M2XDeviceStreamTest(args[0], args[1], args[2], Integer.parseInt(args[3]));
		Thread sendThread = new Thread(worker);
		sendThread.start();

		Console console = System.console();
		if (console != null)
		{
			String consoleinput = console.readLine("Press 2 to stop\n");

			while (sendThread.isAlive())
			{
				if (consoleinput.trim().equalsIgnoreCase("2"))
				{
					CONTINUE = false;
				} else
				{
					LOGGER.error("Invalid input");
					consoleinput = console.readLine("Press 2 to stop\n");
				}
			}
		}
	}
}
